package com.atlassian.confluence.junit3;

import junit.framework.TestCase;
import org.junit.internal.AssumptionViolatedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

/**
 * A convenience abstract subclass of {@link TestCase} that adds timing logging to each test execution. It copies
 * some simple code from {@link TestCase}, and so must be licensed accordingly.
 */
@Deprecated
public abstract class JUnit3TimingTestCase extends TestCase {
    private final Logger log = LoggerFactory.getLogger(getClass());

    private Function<Throwable, Throwable> exceptionTransformer = Function.identity();

    public JUnit3TimingTestCase() {
    }

    public JUnit3TimingTestCase(String testName) {
        super(testName);
    }

    protected void setExceptionTransformer(Function<Throwable, Throwable> exceptionTransformer) {
        this.exceptionTransformer = exceptionTransformer;
    }

    public final void runBare() throws Throwable {
        try {
            Throwable exception = null;
            TestTimer testTimer = TestTimer.start();
            setUp();
            testTimer.endSetUp();
            try {
                runTest();
            } catch (Throwable running) {
                exception = exceptionTransformer.apply(running);
            } finally {
                testTimer.endTest();
                try {
                    tearDown();
                } catch (Throwable tearingDown) {
                    if (exception == null) {
                        exception = tearingDown;
                    }
                }
            }

            testTimer.endTearDown();
            testTimer.log(getName(), exception == null);

            if (exception != null) {
                throw exception;
            }
        } catch (AssumptionViolatedException ex) {
            // if an assumption is violated the test should not be run and
            // should stay green
            if (ex.getMessage() != null) {
                log.warn("{} threw AssumptionViolatedException - skipping test execution due to {}", getName(), ex.getMessage());
            } else {
                log.warn("{} threw AssumptionViolatedException - skipping test execution. No reason given.", getName());
            }
        }
    }
}