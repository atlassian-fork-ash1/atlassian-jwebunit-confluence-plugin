package com.atlassian.confluence.jwebunit;

import com.google.common.base.Throwables;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Provides utilities for accessing private members via reflection.
 */
class ReflectionHelper<T> {
    private final Object obj;
    private final Class<T> returnValueClazz;
    private final Class<?> restrictedMemberClazz;

    ReflectionHelper(Object obj, Class<T> returnValueClazz, Class<?> restrictedMemberClazz) {
        this.obj = obj;
        this.returnValueClazz = returnValueClazz;
        this.restrictedMemberClazz = restrictedMemberClazz;
    }

    T invokePrivateMethod(String methodName, Object... args) {
        try {
            return returnValueClazz.cast(getPrivateMethod(methodName).invoke(obj, args));
        } catch (InvocationTargetException e) {
            throw Throwables.propagate(e.getCause());
        } catch (Exception e) {
            throw Throwables.propagate(e);
        }
    }

    T getPrivateFieldValue(String fieldName) {
        try {
            return returnValueClazz.cast(getPrivateField(fieldName).get(obj));
        } catch (Exception e) {
            throw Throwables.propagate(e);
        }
    }

    void setPrivateFieldValue(String fieldName, T newValue) {
        try {
            getPrivateField(fieldName).set(obj, newValue);
        } catch (Exception e) {
            throw Throwables.propagate(e);
        }
    }

    private Method getPrivateMethod(String methodName) throws NoSuchMethodException {
        final Method method = restrictedMemberClazz.getDeclaredMethod(methodName);
        if (!method.isAccessible()) {
            method.setAccessible(true);
        }
        return method;
    }

    private Field getPrivateField(String fieldName) throws NoSuchFieldException {
        final Field field = restrictedMemberClazz.getDeclaredField(fieldName);
        if (!field.isAccessible()) {
            field.setAccessible(true);
        }
        return field;
    }
}