package com.atlassian.confluence.jwebunit;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebRequest;

import java.net.SocketTimeoutException;

/**
 * This interface aims to externalise all references to proprietary Confluence code from the JWebUnit plugin code.
 */
public interface HtmlUnitConfig {
    int getHttpRequestTimeoutMillis();

    void processSocketTimeoutException(WebRequest webRequest, SocketTimeoutException ex);

    BrowserVersion getBrowserVersion();
}