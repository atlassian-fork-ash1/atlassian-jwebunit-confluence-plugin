package com.atlassian.confluence.jwebunit;

import com.gargoylesoftware.htmlunit.HttpWebConnection;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.WebResponse;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.SocketTimeoutException;

import static org.apache.commons.lang.StringUtils.isNotBlank;

class ConfluenceWebConnection extends HttpWebConnection {
    private static final Logger log = LoggerFactory.getLogger(ConfluenceWebConnection.class);
    private final int maxConnectionsPerRoute;
    private final HtmlUnitConfig htmlUnitConfig;

    public ConfluenceWebConnection(WebClient webClient, int maxConnectionsPerRoute, HtmlUnitConfig htmlUnitConfig) {
        super(webClient);
        this.maxConnectionsPerRoute = maxConnectionsPerRoute;
        this.htmlUnitConfig = htmlUnitConfig;
    }

    @Override
    protected HttpClientBuilder getHttpClientBuilder() {
        return super.getHttpClientBuilder()
                .setMaxConnPerRoute(maxConnectionsPerRoute)
                .setMaxConnTotal(calculateTotalMaxConnections());
    }

    private int calculateTotalMaxConnections() {
        int totalConnections = maxConnectionsPerRoute;
        String baseUrl = System.getProperty("baseurl");
        for (int i = 2; isNotBlank(baseUrl); i++) {
            totalConnections += maxConnectionsPerRoute;
            baseUrl = System.getProperty("baseurl" + i);
        }
        return totalConnections;
    }

    @Override
    public WebResponse getResponse(WebRequest webRequest) throws IOException {
        try {
            return super.getResponse(webRequest);
        } catch (SocketTimeoutException ste) {
            try {
                htmlUnitConfig.processSocketTimeoutException(webRequest, ste);
            } catch (RuntimeException e) {
                log.warn(e.getMessage(), e);
            }
            throw ste;
        }
    }
}
