package com.atlassian.confluence.jwebunit;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.SocketTimeoutException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;


/**
 * Test that the basics of the engine are working.
 */
public class ConfluenceHtmlUnitTestingEngineTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private HtmlForm form;

    @BeforeClass
    public static void initTimeout() {
        ConfluenceHtmlUnitTestingEngine.setGlobalConfig(new HtmlUnitConfig() {

            @Override
            public int getHttpRequestTimeoutMillis() {
                return 0;
            }

            @Override
            public void processSocketTimeoutException(WebRequest webRequest, SocketTimeoutException ex) {
                // do nothing
            }

            @Override
            public BrowserVersion getBrowserVersion() {
                return BrowserVersion.getDefault();
            }
        });
    }

    @Test
    public void testFormFieldReadable() throws IOException, SAXException {
        ConfluenceHtmlUnitTestingEngine engine = new ConfluenceHtmlUnitTestingEngine();
        assertThat(engine._form(), nullValue());

        engine._form(form);
        assertThat(engine._form(), is(form));
    }
}